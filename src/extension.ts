// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode'
import axios from 'axios'
import { Agent } from 'https'

let langKeyProvider: vscode.Disposable | undefined = undefined
let completionItems: vscode.CompletionItem[] = []

let config = {
  host: '',
  lang: '',
  supportLanguageTypes: [] as string[],
  putValueHandler: ''
}

let myStatusBarItem: vscode.StatusBarItem

export async function activate(context: vscode.ExtensionContext) {
  //#region 状态栏
  const myCommandId = 'abpLangKeyHit.showState'
  myStatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100)
  myStatusBarItem.command = myCommandId
  context.subscriptions.push(myStatusBarItem)

  myStatusBarItem.text = 'AbpLang'
  myStatusBarItem.color = 'yellow'
  myStatusBarItem.show()

  // 注册状态栏点击事件
  context.subscriptions.push(
    vscode.commands.registerCommand(myCommandId, async () => {
      if (!langKeyProvider) langKeyProvider = await registerLangKey(context, completionItems)
      else {
        langKeyProvider?.dispose()
        langKeyProvider = undefined
        myStatusBarItem.color = 'red'
      }
    })
  )

  //#endregion

  // 注册配置文件改变事件
  context.subscriptions.push(
    vscode.workspace.onDidChangeConfiguration(async () => {
      const nowConfig = loadConfig()

      if (JSON.stringify(nowConfig) !== JSON.stringify(config)) {
        config = nowConfig
        langKeyProvider = await registerLangKey(context)
      }
    })
  )

  // 注册快捷键触发事件
  context.subscriptions.push(
    vscode.commands.registerCommand('extension.abpLangKeyHit.enabledChange', async (uri) => {
      if (!langKeyProvider) langKeyProvider = await registerLangKey(context, completionItems)
      else {
        langKeyProvider?.dispose()
        langKeyProvider = undefined
        myStatusBarItem.hide()
      }
    })
  )

  //#region init
  config = loadConfig()
  langKeyProvider = await registerLangKey(context)
  //#endregion
}

const loadConfig = () => {
  const host = vscode.workspace.getConfiguration().get<string>('abpLangKeyHit.host')!
  const lang = vscode.workspace.getConfiguration().get<string>('abpLangKeyHit.lang')!
  const supportLanguageTypes = vscode.workspace.getConfiguration().get<string[]>('abpLangKeyHit.supportLanguageTypes')!
  const putValueHandler = vscode.workspace.getConfiguration().get<string>('abpLangKeyHit.putValueHandler')!

  return { host, lang, supportLanguageTypes, putValueHandler }
}

const registerLangKey = async (context: vscode.ExtensionContext, items?: vscode.CompletionItem[]) => {
  langKeyProvider?.dispose()
  myStatusBarItem.color = 'yellow'
  if (!items) {
    const resources = await genLangs(config)
    items = []
    for (const pKey in resources) {
      for (const key in resources[pKey]) {
        const element = resources[pKey][key]
        items.push({
          label: `${pKey}.${key} -- ${element}`,
          insertText: config.putValueHandler?.replace('{{value}}', `${pKey}.${key}`) || `${pKey}.${key}`,
          detail: element,
          kind: vscode.CompletionItemKind.Issue
        })
      }
    }
    completionItems = items
  }

  /** 触发推荐的字符列表 */
  const triggers = [' ']
  const provider = vscode.languages.registerCompletionItemProvider(
    config.supportLanguageTypes!,
    {
      async provideCompletionItems(
        document: vscode.TextDocument,
        position: vscode.Position,
        token: vscode.CancellationToken,
        context: vscode.CompletionContext
      ) {
        return items
      }
    },
    ...triggers
  )
  myStatusBarItem.color = 'white'
  myStatusBarItem.show()
  return provider
  //#endregion
}

const agent = new Agent({
  rejectUnauthorized: false
})

const genLangs = async (request: any) => {
  let host = request.host

  if (host[host.length - 1] != '/') {
    host += '/'
  }

  const { resources } = (await axios
    .get(`${host}api/abp/application-localization?CultureName=${request.lang}`, {
      httpsAgent: agent
    })
    .catch((e) => {
      myStatusBarItem.color = 'red'
    }))!.data

  for (const key in resources) {
    const element = resources[key]

    resources[key] = element.texts
  }

  return resources
}

// This method is called when your extension is deactivated
export function deactivate() {}
