### 介绍

该插件用于 Abp 中的本地化 Key 提示
![alt 属性文本](https://gitee.com/zyknow/abplangkeyhit_vscode_plugin/raw/master/use.png)
### 如何使用

在设置中配置参数，搜索`abpLangKeyHit`

查看右侧`AbpLang`下标状态是否启用
`red`:加载错误
`yellow`:加载数据中
`white`:正常
### 配置参数

详细配置查看设置中的示例

`host`: Abp 后台接口地址(必须配置，否则无法使用插件)
`lang`: Abp 支持的语言值
`supportLanguageType`: 支持的语言类型
`putValueHandler`: 输出值处理

### 快捷键

使用 `ctrl+f10`可以开启或关闭智能提示
